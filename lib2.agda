-- {-# OPTIONS --rewriting #-}

open import Agda.Primitive using (_⊔_ ; Level)
--open import ALevel (Level)

module lib2 where

infixl 5 _×_
infixl 4 _,_
infixl 4 _∙_
infixl 4 _⊎_
infixl 3 _↔_
infix  3 _≡_

postulate
  X Y Z : Set -- propositional variables

data ⊥ : Set where
{-
postulate
  ⊥ : Set
-}

abort : {C : Set} → ⊥ → C
abort ()
{-
postulate
  abort : {C : Set} → ⊥ → C
-}

data ⊤ : Set where
  tt : ⊤
{-
postulate
  ⊤ : Set
  tt : ⊤
-}

-- equality
data _≡_ {i : Level}{A : Set i} : A → A → Set i where
  refl  : (a : A) → a ≡ a
{-
postulate
  _≡_   : ∀{i}{A : Set i} → A → A → Set i
  refl  : ∀{i}{A : Set i}(a : A) → a ≡ a
  subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
-}

subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
subst P (refl a) p = p
-- subst P e p = p   -- does not type check
{-
postulate
  subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
-}


sym   : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
sym (refl a) = refl a
{-
  sym   : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
-}
trans : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
trans (refl a) (refl .a) = refl a
{-
  trans : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
-}
cong  : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f (refl a) = refl (f a)
{-
  cong  : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
-}

-- {-# BUILTIN REWRITE _≡_ #-}

-- for any fixed {i}, A and B there is a separate set A × B
--   so the two arguments of _×_ are *parameters*
record _×_ {i : Level}(A : Set i)(B : Set i) : Set i where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B

open _×_ public

{- alternative definition
data _×_ {i : Level}(A : Set i)(B : Set i) : Set i where
  _,_   : A → B → A × B

proj₁ : ∀{i}{A B : Set i} → A × B → A
proj₁ (a , b) = a

proj₂ : ∀{i}{A B : Set i} → A × B → B
proj₂ (a , b) = b
-}

{-
postulate
  _×_   : ∀{i}(A B : Set i) → Set i
  _,_   : ∀{i}{A B : Set i} → A → B → A × B
  proj₁ : ∀{i}{A B : Set i} → A × B → A
  proj₂ : ∀{i}{A B : Set i} → A × B → B
  ×β₁   : ∀{i}{A B : Set i}{a : A}{b : B} → proj₁ (a , b) ≡ a
  ×β₂   : ∀{i}{A B : Set i}{a : A}{b : B} → proj₂ (a , b) ≡ b

{-# REWRITE ×β₁ #-}
{-# REWRITE ×β₂ #-}
-}

data _⊎_ {i : Level}(A B : Set i) : Set i where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B
{-
postulate
  _⊎_ : ∀{i}(A B : Set i) → Set i
  inj₁ : ∀{i}{A B : Set i} → A → A ⊎ B
  inj₂ : ∀{i}{A B : Set i} → B → A ⊎ B
-}

case : ∀{i j}{A B : Set i}{C : Set j} → A ⊎ B → (A → C) → (B → C) → C
case (inj₁ x) f g = f x
case (inj₂ x) f g = g x
{-
postulate
  case : ∀{i j}{A B : Set i}{C : Set j} → A ⊎ B → (A → C) → (B → C) → C
  ⊎β₁ : ∀{i j}{A B : Set i}{C : Set j}{a : A}{f : A → C}{g : B → C} → case (inj₁ a) f g ≡ f a
  ⊎β₂ : ∀{i j}{A B : Set i}{C : Set j}{b : B}{f : A → C}{g : B → C} → case (inj₂ b) f g ≡ g b

{-# REWRITE ⊎β₁ #-}
{-# REWRITE ⊎β₂ #-}
-}

¬_ : ∀{i} → Set i → Set i
¬ A = A → ⊥

_↔_ : Set → Set → Set
A ↔ B = (A → B) × (B → A)

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
{-
postulate
  ℕ    : Set
  zero : ℕ
  suc  : ℕ → ℕ
-}

rec  : ∀{i}{A : Set i}(az : A)(as : A → A) → ℕ → A
rec az as zero = az
rec az as (suc n) = as (rec az as n)
{-
postulate
  rec  : ∀{i}{A : Set i}(az : A)(as : A → A) → ℕ → A
  ℕrecβzero : ∀{i}{A : Set i}{az : A}{as : A → A}        → rec az as zero ≡ az
  ℕrecβsuc  : ∀{i}{A : Set i}{az : A}{as : A → A}{n : ℕ} → rec az as (suc n) ≡ as (rec az as n)

{-# REWRITE ℕrecβzero #-}
{-# REWRITE ℕrecβsuc #-}
-}

ind  : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))(n : ℕ) → P n
ind P pz ps zero = pz
ind P pz ps (suc n) = ps n (ind P pz ps n)
{-
postulate
  ind  : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))(n : ℕ) → P n
  ℕindβzero : ∀{i}{P : ℕ → Set i}{pz : P zero}{ps : (n : ℕ) → P n → P (suc n)}        → ind P pz ps zero ≡ pz
  ℕindβsuc  : ∀{i}{P : ℕ → Set i}{pz : P zero}{ps : (n : ℕ) → P n → P (suc n)}{n : ℕ} → ind P pz ps (suc n) ≡ ps n (ind P pz ps n)

{-# REWRITE ℕindβzero #-}
{-# REWRITE ℕindβsuc #-}
-}


-- not true: for any fixed n m : ℕ  there is a separate set  n ≤ m
--  so the two arguments of _≤_ are *indices*
data _≤_ : ℕ → ℕ → Set where
  ≤-zero : {n : ℕ} → zero ≤ n
  ≤-suc  : {n m : ℕ} → n ≤ m → suc n ≤ suc m
{-
postulate
  _≤_ : ℕ → ℕ → Set
  ≤-zero : {n : ℕ} → zero ≤ n
  ≤-suc  : {n m : ℕ} → n ≤ m → suc n ≤ suc m
-}

-- dependent pair
record Σ {i j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _∙_    -- \.
  field
    pr₁ : A
    pr₂ : B pr₁

open Σ public

-- use cases of Σ

--- use case 1:  dependent pair

data Bool : Set where
  true : Bool
  false : Bool

if_then_else_ : ∀{i}{A : Set i} → Bool → A → A → A
if true then x else y = x
if false then x else y = y

_⊎'_ : Set → Set → Set
A ⊎' B = Σ Bool (λ b → if b then A else B)
-- here  if_then_else_ : Bool → Set → Set → Set

example₁ example₂ : ℕ ⊎' Bool
example₁ = true ∙ suc zero
example₂ = false ∙ true

--- use case 2:  subtypes (pair an element with a predicate on it)

-- definition with induction
data even : ℕ → Set where
  even-z : even zero
  even-s-s : {n : ℕ} → even n → even (suc (suc n))
{- alternative definition with recursion
even : ℕ → Set
even zero = ⊤
even (suc zero) = ⊥
even (suc (suc n)) = even n
-}

evenℕ = Σ ℕ (λ n → even n)

example₃ : evenℕ
example₃ = suc (suc zero) ∙ even-s-s even-z 

--- use case 3: existential quantification

three = suc (suc (suc zero))

thereIsAnNumberGreaterOrEqualThanThree : Σ ℕ (λ n → three ≤ n)
thereIsAnNumberGreaterOrEqualThanThree
  = three ∙ ≤-suc (≤-suc (≤-suc ≤-zero))
