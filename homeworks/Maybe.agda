{-# OPTIONS --rewriting #-}

module Submission where

infixl 5 _×_
infixl 4 _,_
infixl 4 _⊎_
infixl 3 _↔_

postulate
  X Y Z : Set -- propositional variables
  
  ⊥ : Set
  abort : {C : Set} → ⊥ → C
  
  ⊤ : Set
  tt : ⊤
  
  _×_ : (A B : Set) → Set
  _,_ : {A B : Set} → A → B → A × B
  proj₁ : {A B : Set} → A × B → A
  proj₂ : {A B : Set} → A × B → B

-- equality

postulate
  _≡_   : {A : Set} → A → A → Set
  refl  : {A : Set}(a : A) → a ≡ a
  sym   : {A : Set}{a a' : A} → a ≡ a' → a' ≡ a
  trans : {A : Set}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
  cong  : {A B : Set}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
  subst : {A : Set}(P : A → Set){a a' : A} → a ≡ a' → P a → P a'

infix 3 _≡_

{-# BUILTIN REWRITE _≡_ #-}

postulate
  _⊎_ : (A B : Set) → Set
  inj₁ : {A B : Set} → A → A ⊎ B
  inj₂ : {A B : Set} → B → A ⊎ B
  case : {A B : Set}{C : Set} → A ⊎ B → (A → C) → (B → C) → C
  caseβ₁ : {A B : Set}{C : Set}{a : A}{f : A → C}{g : B → C} → case (inj₁ a) f g ≡ f a
  caseβ₂ : {A B : Set}{C : Set}{b : B}{f : A → C}{g : B → C} → case (inj₂ b) f g ≡ g b

{-# REWRITE caseβ₁ #-}
{-# REWRITE caseβ₂ #-}

¬_ : Set → Set
¬ A = A → ⊥

_↔_ : Set → Set → Set
A ↔ B = (A → B) × (B → A)

Maybe : Set → Set
Maybe = λ A → A ⊎ ⊤

nothing : {A : Set} → Maybe A
nothing = ?

just : {A : Set} → A → Maybe A
just = ?

fmap : {A B : Set} → (A → B) → Maybe A → Maybe B
fmap = ?

bind : {A B : Set} → Maybe A → (A → Maybe B) → Maybe B
bind = ?

join : {A : Set} → Maybe (Maybe A) → Maybe A
join = ?
