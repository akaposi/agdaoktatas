{-# OPTIONS --rewriting #-}

open import Agda.Primitive using (_⊔_)

module lib where

infixl 5 _×_
infixl 4 _,_
infixl 4 _∙_
infixl 4 _⊎_
infixl 3 _↔_

postulate
  X Y Z : Set -- propositional variables
  
  ⊥ : Set
  abort : {C : Set} → ⊥ → C
  
  ⊤ : Set
  tt : ⊤
  
-- equality

postulate
  _≡_   : ∀{i}{A : Set i} → A → A → Set i
  refl  : ∀{i}{A : Set i}(a : A) → a ≡ a
  subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
  -- can be defined with subst:
  sym   : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
  trans : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
  cong  : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'

infix 3 _≡_

{-# BUILTIN REWRITE _≡_ #-}

postulate
  _×_   : ∀{i}(A B : Set i) → Set i
  _,_   : ∀{i}{A B : Set i} → A → B → A × B
  proj₁ : ∀{i}{A B : Set i} → A × B → A
  proj₂ : ∀{i}{A B : Set i} → A × B → B
  ×β₁   : ∀{i}{A B : Set i}{a : A}{b : B} → proj₁ (a , b) ≡ a
  ×β₂   : ∀{i}{A B : Set i}{a : A}{b : B} → proj₂ (a , b) ≡ b

{-# REWRITE ×β₁ #-}
{-# REWRITE ×β₂ #-}

postulate
  _⊎_ : ∀{i}(A B : Set i) → Set i
  inj₁ : ∀{i}{A B : Set i} → A → A ⊎ B
  inj₂ : ∀{i}{A B : Set i} → B → A ⊎ B
  case : ∀{i j}{A B : Set i}{C : Set j} → A ⊎ B → (A → C) → (B → C) → C
  ⊎β₁ : ∀{i j}{A B : Set i}{C : Set j}{a : A}{f : A → C}{g : B → C} → case (inj₁ a) f g ≡ f a
  ⊎β₂ : ∀{i j}{A B : Set i}{C : Set j}{b : B}{f : A → C}{g : B → C} → case (inj₂ b) f g ≡ g b

{-# REWRITE ⊎β₁ #-}
{-# REWRITE ⊎β₂ #-}

¬_ : ∀{i} → Set i → Set i     --  \neg
¬ A = A → ⊥

_↔_ : Set → Set → Set
A ↔ B = (A → B) × (B → A)

postulate
  ℕ    : Set
  zero : ℕ
  suc  : ℕ → ℕ
  rec  : ∀{i}{A : Set i}(az : A)(as : A → A) → ℕ → A
  ind  : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))(n : ℕ) → P n
  ℕrecβzero : ∀{i}{A : Set i}{az : A}{as : A → A}        → rec az as zero ≡ az
  ℕrecβsuc  : ∀{i}{A : Set i}{az : A}{as : A → A}{n : ℕ} → rec az as (suc n) ≡ as (rec az as n)
  ℕindβzero : ∀{i}{P : ℕ → Set i}{pz : P zero}{ps : (n : ℕ) → P n → P (suc n)}        → ind P pz ps zero ≡ pz
  ℕindβsuc  : ∀{i}{P : ℕ → Set i}{pz : P zero}{ps : (n : ℕ) → P n → P (suc n)}{n : ℕ} → ind P pz ps (suc n) ≡ ps n (ind P pz ps n)

{-# REWRITE ℕrecβzero #-}
{-# REWRITE ℕrecβsuc #-}
{-# REWRITE ℕindβzero #-}
{-# REWRITE ℕindβsuc #-}

record Σ {i j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _∙_
  field
    pr₁ : A
    pr₂ : B pr₁

open Σ public
