{-# OPTIONS --rewriting #-}

module TT1 where

open import tt

------------------------------------------------------------------------------
-- empty
------------------------------------------------------------------------------

emptyExample : (A B : Set) → (A → ⊥) → A → B
emptyExample A B na a = abort {C = B} (na a)

------------------------------------------------------------------------------
-- booleans
------------------------------------------------------------------------------

IF_THEN_ELSE_ : ∀{i} → Bool → {C : Set i} → C → C → C
IF b THEN t ELSE f = if[ _ ] b then t else f

IFtrue : ∀{i}{C : Set i}{t f : C} → IF true THEN t ELSE f ≡ t
IFtrue = refl

IFfalse : ∀{i}{C : Set i}{t f : C} → IF false THEN t ELSE f ≡ f
IFfalse = refl

not : Bool → Bool
not = λ b → IF b THEN false ELSE true

nottrue : not true ≡ false
nottrue = refl

notfalse : not false ≡ true
notfalse = refl

and : Bool → Bool → Bool
and = λ a b → IF a THEN b ELSE false

and-lid : ∀ b → and true b ≡ b
and-lid = λ _ → refl

and-rid : ∀ b → and b true ≡ b
and-rid = λ b → if[ (λ b → and b true ≡ b) ] b then refl else refl

or : Bool → Bool → Bool
or = λ a b → IF a THEN true ELSE b

xor : Bool → Bool → Bool
xor = λ a b → IF a THEN not b ELSE b

and-comm : ∀ b b' → and b b' ≡ and b' b
and-comm = λ b b' → if[ (λ b → and b b' ≡ and b' b) ] b
  then if[ (λ b' → and true b' ≡ and b' true) ] b' then refl else refl
  else (if[ (λ b' → and false b' ≡ and b' false) ] b' then refl else refl)

or-comm : ∀ b b' → or b b' ≡ or b' b
or-comm = λ b b' → if[ (λ b → or b b' ≡ or b' b) ] b
  then if[ (λ b' → or true b' ≡ or b' true) ] b' then refl else refl
  else (if[ (λ b' → or false b' ≡ or b' false) ] b' then refl else refl)

xor-comm : ∀ b b' → xor b b' ≡ xor b' b
xor-comm = λ b b' → if[ (λ b → xor b b' ≡ xor b' b) ] b
  then if[ (λ b' → xor true b' ≡ xor b' true) ] b' then refl else refl
  else (if[ (λ b' → xor false b' ≡ xor b' false) ] b' then refl else refl)

------------------------------------------------------------------------------
-- equality
------------------------------------------------------------------------------

sym   : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
sym = {!!}

trans : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
trans = {!!}

cong  : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong = {!!}

subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A} → a ≡ a' → P a → P a'
subst = {!!}

idr : {A : Set}{a a' : A}(p : a ≡ a') → trans p refl ≡ p
idr = {!!}

idl : {A : Set}{a a' : A}(p : a ≡ a') → trans refl p ≡ p
idl = {!!}

-- true ≠ false

true? : Bool → Set
true? = {!!}

true≢false : (true ≡ false) → ⊥
true≢false = {!!}

-- 0 ≠ 1

zero? : ℕ → Set
zero? = {!!}

0≢1 : (zero ≡ suc zero) → ⊥
0≢1 = {!!}

------------------------------------------------------------------------------
-- decidability of equality
------------------------------------------------------------------------------

Decidable : ∀{i} → Set i → Set i
Decidable = λ A → A + ¬ A

≡Bool : Bool → Bool → Bool
≡Bool = {!!}

test≡Bool₁ : ≡Bool true true ≡ true
test≡Bool₁ = {!!}

test≡Bool₂ : ≡Bool false false ≡ true
test≡Bool₂ = {!!}

test≡Bool₃ : ≡Bool true false ≡ false
test≡Bool₃ = {!!}

test≡Bool₄ : ≡Bool false true ≡ false
test≡Bool₄ = {!!}

dec≡Bool : (b b' : Bool) → Decidable (b ≡ b')
dec≡Bool = {!!}

dec≡ℕ : (n n' : ℕ) → Decidable (n ≡ n')
dec≡ℕ = {!!}

dec≡+ : ∀{i}{A B : Set i}
        (dec≡A : (a a' : A) → Decidable (a ≡ a'))
        (dec≡B : (b b' : B) → Decidable (b ≡ b'))
      → (w w' : A + B) → Decidable (w ≡ w')
dec≡+ = {!!}

------------------------------------------------------------------------------
-- lists
------------------------------------------------------------------------------

l1 : List Bool
l1 = true ∷l true ∷l true ∷l true ∷l true ∷l []l

l2 : List Bool
l2 = []l


lengthl : {A : Set} → List A → ℕ
lengthl = {!!}

mapsl : {A B : Set} → List (A → B) → List A → List B
mapsl = {!!}

headl : {A : Set} → List A → A
headl = {!!}

taill : {A : Set} → List A → List A
taill = {!!}

headl' : {A : Set}(xs : List A) → ¬ (lengthl xs ≡ zero) → A
headl' = {!!}

------------------------------------------------------------------------------
-- vectors
------------------------------------------------------------------------------

n5 : ℕ
n5 = suc (suc (suc (suc (suc zero))))

v1 : Vec Bool n5
v1 = true ∷ true ∷ true ∷ true ∷ true ∷ []

v2 : Vec Bool zero
v2 = []

maps : {n : ℕ}{A B : Set} → Vec (A → B) n → Vec A n → Vec B n
maps = {!!}

head : {n : ℕ}{A : Set} → Vec A (suc n) → A
head = {!!}

tail : {n : ℕ}{A : Set} → Vec A (suc n) → A
tail = {!!}

------------------------------------------------------------------------------
-- isomorphism of Bool and ⊤ + ⊤
------------------------------------------------------------------------------

_≅_ : Set → Set → Set -- \~=
A ≅ B = Σ (A → B) λ f → Σ (B → A) λ g →
          ((x : A) → g (f x) ≡ x) × ((y : B) → f (g y) ≡ y)

w : Bool ≅ (⊤ + ⊤)
w =  f
  , g
  , (λ x → {!g (f true)!})
  , {!!}
  where
    f : Bool → ⊤ + ⊤
    f = λ b → if[ (λ _ → ⊤ + ⊤) ] b then inj₁ tt else inj₂ tt

    g : ⊤ + ⊤ → Bool
    g = λ x → case (λ _ → Bool) (λ _ → true) (λ _ → false) x

--         f             g
-- true  |---> inj₁ tt |---> true
-- false |---> inj₂ tt |---> false

--           g           f
-- inj₁ tt |---> true  |---> inj₁ tt
-- inj₂ tt |---> false |---> inj₂ tt


------------------------------------------------------------------------------
-- the two different isomorphisms between Bool and Bool
------------------------------------------------------------------------------

idiso : Bool ≅ Bool
idiso = {!!}

notiso : Bool ≅ Bool
notiso = {!!}

------------------------------------------------------------------------------
-- Curry - Uncurry
------------------------------------------------------------------------------

Ση : ∀{i j}{A : Set i}{B : A → Set j}(w : Σ A B) → (proj₁ w , proj₂ w) ≡ w
Ση {A = A}{B} = IndΣ (λ w → proj₁ w , proj₂ w ≡ w) (λ _ _ → refl)

curryiso : ∀{A B C : Set} → (A → B → C) ≅ (A × B → C)
curryiso {A}{B}{C}
  = (λ h ab → h (proj₁ ab)(proj₂ ab))
  , (λ h a b → h (a , b))
  , (λ h → refl) -- funext λ a → funext λ b → {!!})
  , λ h → funext λ ab → cong (λ z → h z) (Ση ab)

------------------------------------------------------------------------------
-- random exercises
------------------------------------------------------------------------------

congid : ∀{ℓ}{A : Set ℓ}{x y : A}(p : x ≡ y) → cong (λ x → x) p ≡ p
congid = {!!}

≡inv : ∀{ℓ}{A : Set ℓ} {x y : A} (p : x ≡ y) → (trans p (sym p)) ≡ refl
≡inv = {!!}

,=0 : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{b : B a}{b' : B a'}
   → _≡_ {A = Σ A B} (a , b) (a' , b') → a ≡ a'
,=0 = {!!}

,=1 : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{b : B a}{b' : B a'}
      (p : (a , b) ≡ (a' , b')) → subst B (,=0 p) b ≡ b'
,=1 = {!!}

,= : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{b : B a}{b' : B a'}
     (p : a ≡ a') → subst B p b ≡ b' → _≡_ {A = Σ A B} (a , b) (a' , b')
,= = {!!}



-- TODO: set → h1
-- TODO: Hedberg
