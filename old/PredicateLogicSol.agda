module PredicateLogic where

------------------------------------------------------------------------------
-- stdlib
------------------------------------------------------------------------------

infixl 4 _,_
infixl 5 _∧_
infixl 4 _∨_
infixl 3 _↔_

record ⊤ : Set where
  constructor tt

data ⊥ : Set where

abort : (C : Set) → ⊥ → C
abort C ()

data _∨_ (A B : Set) : Set where
  inj₁ : A → A ∨ B
  inj₂ : B → A ∨ B

case : {A B : Set}(C : Set) → (A → C) → (B → C) → A ∨ B → C
case C f g (inj₁ a) = f a
case C f g (inj₂ b) = g b

¬_ : Set → Set
¬ A = A → ⊥

record ∃ (A : Set) (B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open ∃

_∧_ : Set → Set → Set
A ∧ B = ∃ A λ _ → B

_↔_ : Set → Set → Set
A ↔ B = (A → B) ∧ (B → A)

postulate
  X Y Z : Set
  M N : Set
  P Q : M → Set
  R : M → N → Set

------------------------------------------------------------------------------
-- Forall
------------------------------------------------------------------------------

-- ∀(x : M) → P x   -- \forall

--     x : M ⊢ p : P x                  f : ∀(x : M) → P x      m : M
-- ------------------------intro        -----------------------------elim
-- λ x → p : ∀(x : M) → P x                       f m : P m

l1 : (∀(x : M) → P x ∧ Q x) ↔ (∀(x : M) → P x) ∧ (∀(x : M) → Q x)
l1 = (λ f → (λ m → proj₁ (f m)) , (λ m → proj₂ (f m))) , (λ f m → (proj₁ f m) , (proj₂ f m))

l2 : ¬ ¬ (∀(x : M) → P x) → ∀(x : M) → ¬ ¬ (P x)
l2 = λ h m npm → h (λ f → npm (f m))

------------------------------------------------------------------------------
-- Exists
------------------------------------------------------------------------------

-- ∃ M (λ x → P x)   -- \exists

-- m : M        p : P m           w : ∃ M λ x → P x        w : ∃ M λ x → P x
-- -----------------------intro   -----------------elim1  ---------------------elim2  
-- (m , p) : ∃ M λ x → P x          proj₁ w : M           proj₂ w : P (proj₁ m)           


l3 : (∃ M λ x → ¬ P x) → ¬ ∀(x : M) → P x
l3 = λ w f → proj₂ w (f (proj₁ w))

l4 : (¬ ∃ M λ x → P x) → ∀(x : M) → ¬ P x
l4 = λ h m pm → h (m , pm)

l5 : (∀(x : M) → ¬ P x) → ¬ ∃ M λ x → P x
l5 = λ h mp → h (proj₁ mp) (proj₂ mp)

l6 : (∃ M λ x → ∀(y : N) → R x y) → ∀(y : N) → ∃ M λ x → R x y
l6 = λ w n → proj₁ w , proj₂ w n

-- axiom of choice

AC : (∀(x : M) → ∃ N λ y → R x y) → ∃ (M → N) λ f → ∀(x : M) → R x (f x)
AC = λ h → (λ m → proj₁ (h m)) , (λ m → proj₂ (h m))

-- these need classical logic

module classical where

  postulate
    LEM : (X : Set) → X ∨ ¬ X   -- magic

  DNP : (X : Set) → ¬ ¬ X → X
  DNP = λ X → λ nnx → case X (λ x → x) (λ nx → abort X (nnx nx)) (LEM X)

  l7 : (¬ ∃ M (λ x → ¬ P x)) → ∀(x : M) → P x
  l7 = λ w x → DNP (P x) (λ npx → w (x , npx))

  pub : ∃ M (λ m → ⊤) → ∃ M λ x → P x → ∀(y : M) → P y
  pub = λ v → case (∃ M λ x → P x → ∀(y : M) → P y)
                   (λ w → proj₁ w , (λ p y → abort (P y) (proj₂ w p)))
                   (λ w → proj₁ v , (λ v → l7 w))
                   (LEM (∃ M λ x → ¬ P x))

  l8 : (¬ ∀(x : M) → P x) → ∃ M λ x → ¬ P x
  l8 = λ h → {!!}

