
module Natural where

------------------------------------------------------------------------------
-- stdlib
------------------------------------------------------------------------------

infixl 4 _,_
infixl 5 _∧_
infixl 4 _∨_
infixl 3 _↔_

record ⊤ : Set where
  constructor tt

data ⊥ : Set where

abort : (C : Set) → ⊥ → C
abort C ()

data _∨_ (A B : Set) : Set where
  inj₁ : A → A ∨ B
  inj₂ : B → A ∨ B

case : {A B : Set}(C : Set) → (A → C) → (B → C) → A ∨ B → C
case C f g (inj₁ a) = f a
case C f g (inj₂ b) = g b

¬_ : Set → Set
¬ A = A → ⊥

record ∃ (A : Set) (B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open ∃

_∧_ : Set → Set → Set
A ∧ B = ∃ A λ _ → B

_↔_ : Set → Set → Set
A ↔ B = (A → B) ∧ (B → A)

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

Indℕ : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))(n : ℕ) → P n
Indℕ P pz ps zero = pz
Indℕ P pz ps (suc n) = ps n (Indℕ P pz ps n)

Recℕ : (A : Set)(az : A)(as : A → A) → ℕ → A
Recℕ A az as zero = az
Recℕ A az as (suc n) = as (Recℕ A az as n)

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x

infix 3 _≡_

sym : {A : Set}{x y : A} → x ≡ y → y ≡ x
sym (refl _) = refl _

transport : {A : Set}(P : A → Set){a b : A} → a ≡ b → P a → P b
transport P (refl x) u = u

cong : {A B : Set}(f : A → B){a b : A} → a ≡ b → f a ≡ f b
cong f (refl a) = refl (f a)

data Bool : Set where
  true : Bool
  false : Bool

if_then_else_ : {A : Set} → Bool → A → A → A
if true then a else b = a
if false then a else b = b

Ifthenelse : ∀{i}(P : Bool → Set i) → P true → P false → (b : Bool) → P b
Ifthenelse P t f true = t
Ifthenelse P t f false = f

------------------------------------------------------------------------------
-- Equality
------------------------------------------------------------------------------

-- A : Set   a b : A     A : Set    a : A
-- -----------------     ----------------
--    a ≡ b : Set         refl a : a ≡ a

--   p : a ≡ b
-- -------------
-- sym p : b ≡ a
--
-- sym (refl a) = refl a

-- P : A → Set    r : a ≡ b    u : P a
-- -----------------------------------
--      transport P r u : P b
--
-- transport P (refl a) u = u

-- f : A → B    r : a ≡ b
-- ----------------------
--  cong f r : f a ≡ f b
--
-- cong f (refl a) = refl (f a)

trans : (A : Set)(x y z : A) → x ≡ y → y ≡ z → x ≡ z
trans = λ A x y z p q → transport (λ a → x ≡ a) {y}{z} q p

------------------------------------------------------------------------------
-- Natural numbers
------------------------------------------------------------------------------

--                         n : ℕ
-- -------   --------    ---------
-- ℕ : Set   zero : ℕ    suc n : ℕ
--
-- A : Set   z : A   s : A → A
-- ---------------------------
--      Recℕ A z s : ℕ → A
--
-- Recℕ A z s zero = z
-- Recℕ A z s (suc n) = s (Recℕ A z s n)

_+_ : ℕ → ℕ → ℕ
_+_ = λ x y → Recℕ ℕ y (λ m → suc m) x -- x szerinti primitiv rekurzio

-- Recℕ ℕ y (λ m → suc m) zero = y

{-
3 + 19 = _+_ 3 19
       = ((λ x → (λ y → Recℕ ℕ y (λ m → suc m) x)) 3) 19
          (λ x → t                               ) 3
       = ((λ y → Recℕ ℕ y (λ m → suc m) 3)          ) 19
       = Recℕ ℕ 19 (λ m → suc m) 3
       = Recℕ ℕ 19 suc (suc (suc (suc zero)))
       = suc (Recℕ ℕ 19 suc (suc (suc zero)))
       = suc (suc (Recℕ ℕ 19 suc (suc zero)))
       = suc (suc (suc (Recℕ ℕ 19 suc zero)))
       = suc (suc (suc 19))
       = 22
-}

_∘_ : {A B C : Set} → (B → C) → (A → B) → (A → C)  -- \o1  \circ
_∘_ = λ g f → λ x → g (f x)

infixr 3 _∘_
infixl 5 _+_
infixl 6 _*_

test : 13 + 22 ≡ 35
test = refl 35

3*_+5 : ℕ → ℕ
3*_+5 = Recℕ ℕ 5 (_+_ 3) -- (suc ∘ suc ∘ suc)

test1 : 3* 4 +5 ≡ 17
test1 = refl _

test2 : 3* 0 +5 ≡ 5
test2 = refl _

leftunit : ∀(n : ℕ) → 0 + n ≡ n
leftunit = refl

_*_ : ℕ → ℕ → ℕ
_*_ = λ x y → Recℕ ℕ zero (λ m → y + m) x    -- _+_ y

test3 : 3 * 4 ≡ 12
test3 = refl _

test4 : 100 * 100 ≡ 10000
test4 = refl _

-- P : ℕ → Set   z : P zero   s : (n : ℕ) → P n → P (suc n)
-- --------------------------------------------------------
--                 Indℕ P z s n : ∀(n : ℕ) → P n
--
-- Indℕ P z s zero = z
-- Indℕ P z s (suc n) = s (Indℕ P z s n)

rightunit : ∀(n : ℕ) → n + 0 ≡ n
rightunit = Indℕ (λ n → n + 0 ≡ n) (refl 0) (λ n p → cong suc p)

assoc : ∀(a b c : ℕ) → (a + b) + c ≡ a + (b + c)
assoc = λ a b c
      → Indℕ (λ x → (x + b) + c ≡ x + (b + c))
             (refl (b + c))
             (λ n p → cong suc p)
             a

pred : ℕ → ℕ
pred = Indℕ _ zero (λ n _ → n)

test5 : pred 5 ≡ 4
test5 = refl 4

test6 : pred 0 ≡ 0
test6 = refl 0

-- rekurzio

------------------------------------------------------------------------------
-- Booleans
------------------------------------------------------------------------------

-- -----------    ------------
-- true : Bool    false : Bool
--
-- b : Bool    t : A    f : A
-- --------------------------
--   if b then t else f : A
--
-- if true then t else f = t
-- if false then t else f = t

-- and, or, xor with proofs

not : Bool → Bool
not = λ b → if b then false else true

and : Bool → Bool → Bool
and = λ b b' → if b then b' else false

or : Bool → Bool → Bool
or = {!!}

xor : Bool → Bool → Bool
xor = {!!}

and-comm : ∀ b b' → and b b' ≡ and b' b
and-comm = Ifthenelse _
  (Ifthenelse _ (refl _) (refl _))
  (Ifthenelse _ (refl _) (refl _))

or-comm : ∀ b b' → or b b' ≡ or b' b
or-comm = {!!}

xor-comm : ∀ b b' → xor b b' ≡ xor b' b
xor-comm = {!!}

-- P : Bool → Set    t : P true    f : P false    b : Bool
-- -------------------------------------------------------
--                 Ifthenelse P t f b : P b
--
-- Ifthenelse P t f true = t
-- Ifthenelse P t f false = f

-- true ≠ false

true? : Bool → Set
true? = Ifthenelse _ ⊤ ⊥

true≢false : (true ≡ false) → ⊥
true≢false = λ p → transport (λ b → true? b) p tt

