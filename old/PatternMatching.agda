module PatternMatching where

------------------------------------------------------------------------------
-- Bool
------------------------------------------------------------------------------

data Bool : Set where
  true : Bool
  false : Bool

-- pattern matching
not : Bool → Bool
not true = false
not false = true

-- pattern matching
and : Bool → Bool → Bool
and true b = b
and _ _ = false

-- pattern matching
and1 : Bool → Bool → Bool
and1 true true = true
and1 _ _ = false

and2 : Bool → Bool → Bool
and2 true true = true
and2 true false = false
and2 false true = false
and2 false false = false

-- short circuit test
-- and true x    vs.   and1 true x
-- and false x   vs.   and2 false x

postulate
  x : Bool

-- definition of the eliminator: pattern matching
if_then_else_ : ∀{i}{A : Set i} → Bool → A → A → A
if true then t else f = t
if false then t else f = f

-- definition of the eliminator: pattern matching
Ifthenelse : ∀{i}(P : Bool → Set i) → P true → P false → (b : Bool) → P b
Ifthenelse P pt pf true = pt
Ifthenelse P pt pf false = pf

-- with eliminator
not' : Bool → Bool
not' b = if b then false else true

-- with eliminator
and' : Bool → Bool → Bool
and' a b = if a then b else false

or or' : Bool → Bool → Bool

-- pattern matching
or true b = true
or false b = b

-- eliminator
or' = λ a b → if a then true else b
-- or' a b = if a then true else b

------------------------------------------------------------------------------
-- Natural
------------------------------------------------------------------------------

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

infixl 5 _+_
infixl 7 _*_
 
-- pattern matching
odd : ℕ → Bool
odd zero = false
odd (suc n) = not (odd n)

-- odd 3 = odd (suc (suc (suc zero))) = not (odd (suc (suc zero)))
--  = not (not (odd (suc zero))) = not (not (not (odd zero))) = not (not (not false))
-- not (not true) = not false = true

-- pattern matching
_+_ : ℕ → ℕ → ℕ
zero + b = b
(suc a) + b = suc (a + b)

-- definition of the eliminator: pattern matching
Recℕ : (A : Set)(az : A)(as : A → A) → ℕ → A
Recℕ A az as zero = az
Recℕ A az as (suc n) = as (Recℕ A az as n)

-- definition of the eliminator: pattern matching
Indℕ : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n)) → (n : ℕ) → P n
Indℕ P pz ps zero = pz
Indℕ P pz ps (suc n) = ps n (Indℕ P pz ps n)

-- eliminator
_+'_ : ℕ → ℕ → ℕ
_+'_ = λ a b → Recℕ ℕ b suc a

-- eliminator
odd' : ℕ → Bool
odd' = Recℕ Bool false not

-- n ↦ 2*n+1

-- pattern matching    C-c C-c
2*_+1 : ℕ → ℕ
2* zero +1 = 1
2* suc n +1 = 2 + 2* n +1

-- eliminator
2*_+1' : ℕ → ℕ
2*_+1' = Recℕ ℕ 1 (_+_ 2) -- (λ m → suc (suc m)) (λ m → 2 + m)

-- pattern matching
_*_ : ℕ → ℕ → ℕ
zero * b = 0
suc a * b = b + (a * b)

_*'_ : ℕ → ℕ → ℕ
a *' b = Recℕ ℕ 0 (λ a*b → b + a*b) a

------------------------------------------------------------------------------
-- Empty
------------------------------------------------------------------------------

data ⊥ : Set where

-- definition of the eliminator: pattern matching
abort : (C : Set) → ⊥ → C
abort C ()

¬_ : Set → Set
¬ A = A → ⊥     -- ¬_ = λ A → (A → ⊥)

------------------------------------------------------------------------------
-- Singleton
------------------------------------------------------------------------------

data ⊤' : Set where
  tt : ⊤'

-- another version:
record ⊤ : Set where
  constructor tt

-- no eliminator

------------------------------------------------------------------------------
-- Equality
------------------------------------------------------------------------------

infix 3 _≡_

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x

-- pattern matching
sym : {A : Set}{x y : A} → x ≡ y → y ≡ x
sym (refl x) = refl x

sym' : (A : Set)(x y : A) → x ≡ y → y ≡ x
sym' A x .x (refl .x) = refl x

-- pattern matching
trans : (A : Set)(x y z : A) → x ≡ y → y ≡ z → x ≡ z
trans A x y .y p (refl .y) = p

-- pattern matching
transport : {A : Set}(P : A → Set){a b : A} → a ≡ b → P a → P b
transport P (refl x) u = u

-- with transport
trans' : (A : Set)(x y z : A) → x ≡ y → y ≡ z → x ≡ z
trans' A x y z p q = transport (λ a → x ≡ a) {y}{z} q p

-- pattern matching
cong : {A B : Set}(f : A → B){a b : A} → a ≡ b → f a ≡ f b
cong f (refl a) = refl (f a)

------------------------------------------------------------------------------
-- Bool and equality
------------------------------------------------------------------------------

-- pattern matching
P : Bool → Set
P = {!!}

-- eliminator
P' : Bool → Set
P' b = if b then ⊤ else ⊥

-- pattern matching
true≢false : true ≡ false → ⊥
true≢false p = {!!}

-- eliminator
true≢false' : true ≡ false → ⊥
true≢false' p = {! transport {Bool} P' p tt !}

------------------------------------------------------------------------------
-- Nat and equality
------------------------------------------------------------------------------

-- 2*_+1
2*+1-lemma : ∀ n → 2* n +1 ≡ 2 * n + 1
2*+1-lemma = Indℕ _ (refl 1) (λ n p → {!cong (λ n → 2 + n) p!})
-- λ n → 2* n +1 ≡ 2 * n + 1


-- eliminator
+-assoc : ∀ a b c → (a + b) + c ≡ a + (b + c)
+-assoc a b c = Indℕ (λ a → (a + b) + c ≡ a + (b + c))
                     (refl (b + c))
                     (λ n p → cong suc p)
                     a

-- pattern matching
+-assoc' : ∀ a b c → (a + b) + c ≡ a + (b + c)
+-assoc' zero b c = refl (b + c)
+-assoc' (suc a) b c = cong suc (+-assoc' a b c)

left-unit : ∀ a → 0 + a ≡ a
left-unit = {!!}

right-unit : ∀ a → a + 0 ≡ a
right-unit = {!!}

-- pattern matching
0≠1 : ¬ (0 ≡ 1)
0≠1 = {!!}

-- eliminator (similarly to true≢false')
0≠1' : ¬ (0 ≡ 1)
0≠1' = {!!}

------------------------------------------------------------------------------
-- Lists
------------------------------------------------------------------------------

infixr 5 _∷_

data List (A : Set) : Set where
  nil : List A
  _∷_ : A → List A → List A

-- examples

l1 : List Bool
l1 = true ∷ true ∷ true ∷ true ∷ true ∷ nil

l2 : List Bool
l2 = nil

infixr 5 _++_

-- pattern matching
_++_ : {A : Set} → List A → List A → List A
_++_ = {!!}

-- pattern matching
length : {A : Set} → List A → ℕ
length = {!!}

-- pattern matching
maps : {A B : Set} → List (A → B) → List A → List B
maps = {!!}

-- pattern matching
head : {A : Set} → List A → A
head = {!!}

-- pattern matching
tail : {A : Set} → List A → List A
tail = {!!}

------------------------------------------------------------------------------
-- Vectors
------------------------------------------------------------------------------

data Vec (A : Set) : ℕ → Set where
  nil : Vec A zero
  _∷_ : ∀ {n} → A → Vec A n → Vec A (suc n)

-- examples

v1 : Vec Bool 5
v1 = true ∷ true ∷ true ∷ true ∷ true ∷ nil

v2 : Vec Bool 0
v2 = nil

-- pattern matching
mapsV : {n : ℕ}{A B : Set} → Vec (A → B) n → Vec A n → Vec B n
mapsV = {!!}

-- pattern matching
headV : {n : ℕ}{A : Set} → Vec A (suc n) → A
headV = {!!}

-- pattern matching
tailV : {n : ℕ}{A : Set} → Vec A (suc n) → A
tailV = {!!}

------------------------------------------------------------------------------
-- Propositional logic
------------------------------------------------------------------------------

-- no pattern matching on functions

-- polymorphic identity function
id : {A : Set} → A → A
id = {!!}

-- an example lemma
l10 : {X : Set} → X → ¬ ¬ X
l10 = {!!}

infixl 4 _⊎_ -- \u+

data _⊎_ (A B : Set) : Set where -- same as _∨_
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

-- pattern matching
l21 : {X : Set} → X ⊎ X → X
l21 = {!!}

-- pattern matching
l23 : {X Y : Set} → (Y ⊎ X) → (X ⊎ Y)
l23 = {!!}

-- definition of the eliminator: pattern matching
case : {A B : Set}(C : Set) → (A → C) → (B → C) → A ⊎ B → C
case C f g (inj₁ a) = f a
case C f g (inj₂ b) = g b

------------------------------------------------------------------------------
-- Predicate logic
------------------------------------------------------------------------------

infixl 4 _,_
infixl 5 _×_ -- \times
infixl 3 _↔_

record Σ (A : Set) (B : A → Set) : Set where  -- same as ∃
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ

-- abbreviation
_×_ : Set → Set → Set -- same as _∧_
A × B = Σ A λ _ → B

-- abbreviation
_↔_ : Set → Set → Set
A ↔ B = (A → B) × (B → A)

-- pattern matching

l24 : {X Y Z : Set} → (X ⊎ Y) → Z → (X → Z) × (Y → Z)
l24 = {!!}
