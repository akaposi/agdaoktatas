
module Inductive where

-- Induktív definíciók
-- docs: https://agda.readthedocs.io/en/v2.5.2/language/data-types.html

-- paraméterek : kettőspont bal oldalán, minden konstruktor visszatérési típusában ugyanaz
-- indexek     : kettőspont jobb oldalán, változhat konstruktorok visszatérési típusában

-- Függő mintaillesztés: az indexekben szereplő típusváltozókat megoldja/behelyettesíti
-- annak függvényében, hogy melyik konstruktort vizsgáljuk éppen.

-- Egyszerű induktív definíciók
data Bool : Set where
  true  : Bool
  false : Bool

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

-- példák egyszerű mintaillesztésre
and : Bool → Bool → Bool
and true  b' = b'
and false b' = false

infixl 5 _+_
_+_ : ℕ → ℕ → ℕ
zero  + b = b
suc a + b = suc (a + b) -- rekurzív hívás _+_-al!

-- Rekurzív hívások csak strukturálisan kisebb argumentumon lehetségesek,
-- mint ahogy _+_-nál is; ezzel kiszűrjuk a végtelen loop-okat.
-- Végetelen loop-okat azért nem engedélyezünk, mivel segítségükkel
-- akármilyen típusnak tudunk értéket adni, ami logikailag inkonzisztens:

loop : {A : Set} → A
loop = loop  -- Agda: "termination checking failed"

-- Induktív def. két Bool indexxel
data BoolEq : Bool → Bool → Set where
  trueEq  : BoolEq true true
  falseEq : BoolEq false false

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x
infix 3 _≡_

-- Függő mintaillesztés (refl _)-re:
sym : {A : Set}{x y : A} → x ≡ y → y ≡ x
sym (refl z) = refl z  -- jobb oldal típusa: "z ≡ z" lesz helyettesítés után

-- A bal oldalon (refl z) típusa "z ≡ z", viszont a típusdeklaráció szerint
-- az argumentumnak "x ≡ y" a típusa. Agda egységesíti a kettőt, azaz
-- létrehoz egy olyan változóhelyettesítést, ami mellett a két típus egyenlő lesz.
-- Ebben az esetben "x := z" és "y := z" helyettesítés jön létre.

cong : ∀ {A B}{x y}(f : A → B) → x ≡ y → f x ≡ f y
cong f (refl x) = refl (f x)

trans : {A : Set}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans (refl _) q = q

-- példa egyenlőségi bizonyításra mintaillesztéssel és rekurzív hívással:
+-assoc : ∀ a b c → (a + b) + c ≡ a + (b + c)
+-assoc zero    b c = refl _
+-assoc (suc a) b c = cong suc (+-assoc a b c)

-- Agda mintaillesztésnél megnézi, hogy milyen konstruktorok lehetségesek. Ebben
-- Az esetben nincs lehetséges eset, mivel "_≡_" konstruktora "refl", viszont
-- "refl x" típusa "x ≡ x", és ezt nem lehet egységesíteni "true ≡ false"-al.
-- Tehát nincs egy vizsgálandó eset sem, és a "()" ún. abszurd minta elég
-- a bizonyításhoz.
data ⊥ : Set where

true≢false : true ≡ false → ⊥
true≢false ()

-- A listák típusa paraméterezve van egy "A" típussal. Mivel paraméter, ezért
-- minden konstruktor definíciónál automatikusan scope-ban van, és minden konstruktor
-- visszatérési típusában csak uniform "A" szerepelhet.
data List (A : Set) : Set where
  nil  : List A
  cons : A → List A → List A

-- "Vec A n" az "n" hosszú "A" elemeket tartalmazó listák típusa.
-- Itt az "A" paraméter, míg az "n" index.

data Vec (A : Set) : ℕ → Set where
  nil : Vec A zero
  _∷_ : ∀ {n} → A → Vec A n → Vec A (suc n)

infixr 5 _∷_

-- Példák Vec-re

v1 : Vec Bool 5
v1 = true ∷ true ∷ true ∷ true ∷ true ∷ nil

v2 : Vec Bool 0
v2 = nil

-- Típushiba, mivel a megadott Vec két elemet tartalmaz, a típusa
-- viszont 3-at specifikál.
-- v3 : Vec Bool 3
-- v3 = true ∷ true ∷ nil

-- Vec-re a "map" függvény típusa megszorítja az implementációt:
-- az output lista hossza ugyanaz kell, hogy legyen, mint az input hossza.
map : {n : ℕ}{A B : Set} → (A → B) → Vec A n → Vec B n
map f nil      = nil
map f (x ∷ xs) = f x ∷ map f xs

-- Az alábbi "zipWith" két azonos hosszúságú Vec-et kombinál egy függvény
-- segítségével elemenként.
-- Elvileg négy eset lenne, (mivel mindkét Vec lehetne "nil" vagy "_∷_")
-- de mivel Agda tudja, hogy a két input lista hossza megegyezik, ezért
-- csak a nil-nil és _∷_-_∷_ eseteket engedélyezi.
zipWith : {A B C : Set}{n : ℕ} → (A → B → C) → Vec A n → Vec B n → Vec C n
zipWith f nil      nil      = nil
zipWith f (x ∷ xs) (y ∷ ys) = f x y ∷ zipWith f xs ys



