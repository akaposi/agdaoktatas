module Natural where

------------------------------------------------------------------------------
-- stdlib
------------------------------------------------------------------------------

infixl 4 _,_
infixl 5 _∧_
infixl 4 _∨_
infixl 3 _↔_

record ⊤ : Set where
  constructor tt

data ⊥ : Set where

abort : (C : Set) → ⊥ → C
abort C ()

data _∨_ (A B : Set) : Set where
  inj₁ : A → A ∨ B
  inj₂ : B → A ∨ B

case : {A B : Set}(C : Set) → (A → C) → (B → C) → A ∨ B → C
case C f g (inj₁ a) = f a
case C f g (inj₂ b) = g b

¬_ : Set → Set
¬ A = A → ⊥

record ∃ (A : Set) (B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open ∃

_∧_ : Set → Set → Set
A ∧ B = ∃ A λ _ → B

_↔_ : Set → Set → Set
A ↔ B = (A → B) ∧ (B → A)

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

Indℕ : ∀{i}(P : ℕ → Set i)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))(n : ℕ) → P n
Indℕ P pz ps zero = pz
Indℕ P pz ps (suc n) = ps n (Indℕ P pz ps n)

Recℕ : (A : Set)(az : A)(as : A → A) → ℕ → A
Recℕ A az as zero = az
Recℕ A az as (suc n) = as (Recℕ A az as n)

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x

infix 4 _≡_

sym : {A : Set}{x y : A} → x ≡ y → y ≡ x
sym (refl _) = refl _

transport : {A : Set}(P : A → Set){a b : A} → a ≡ b → P a → P b
transport P (refl x) u = u

cong : {A B : Set}(f : A → B){a b : A} → a ≡ b → f a ≡ f b
cong f (refl a) = refl (f a)

data Bool : Set where
  true false : Bool

if_then_else_ : {A : Set} → Bool → A → A → A
if true then a else b = a
if false then a else b = b

Ifthenelse : ∀{i}(P : Bool → Set i) → P true → P false → (b : Bool) → P b
Ifthenelse P t f true = t
Ifthenelse P t f false = f

------------------------------------------------------------------------------
-- Equality
------------------------------------------------------------------------------

-- A : Set   a b : A     A : Set    a : A
-- -----------------     ----------------
--    a ≡ b : Set         refl a : a ≡ a

--   p : a ≡ b
-- -------------
-- sym p : b ≡ a
--
-- sym (refl a) = refl a

-- P : A → Set    r : x ≡ y    u : P x
-- -----------------------------------
--      transport P r u : P y
--
-- transport P (refl a) u = u

-- f : A → B    r : a ≡ b
-- ----------------------
--  cong f r : f a ≡ f b
--
-- cong f (refl a) = refl (f a)

trans : (A : Set)(x y z : A) → x ≡ y → y ≡ z → x ≡ z
trans A x y z p q = transport (λ q → x ≡ q) q p

------------------------------------------------------------------------------
-- Natural numbers
------------------------------------------------------------------------------

--               n : ℕ
-- --------    ---------
-- zero : ℕ    suc n : ℕ
-- 
-- A : Set   z : A   s : A → A   n : ℕ
-- -----------------------------------
--           Recℕ A z s n : A
-- 
-- Recℕ A z s zero = z
-- Recℕ A z s (suc n) = s (Recℕ A z s n)

_+_ : ℕ → ℕ → ℕ
_+_ = λ x → Recℕ (ℕ → ℕ) (λ y → y) (λ f y → suc (f y)) x

test : 13 + 22 ≡ 35
test = refl 35

3*_+5 : ℕ → ℕ
3* n +5 = Recℕ ℕ 5 (λ m → suc (suc (suc m))) n

test1 : 3* 4 +5 ≡ 17
test1 = refl 17

test2 : 3* 0 +5 ≡ 5
test2 = refl _

leftunit : ∀(n : ℕ) → 0 + n ≡ n
leftunit = {!!}

_*_ : ℕ → ℕ → ℕ
_*_ = {!!}

-- P : ℕ → Set   z : P zero   s : (n : ℕ) → P n → P (suc n)   n : ℕ
-- ----------------------------------------------------------------
--                        Indℕ P z s n : P n
-- 
-- Indℕ P z s zero = z
-- Indℕ P z s (suc n) = s (Indℕ P z s n)

rightunit : ∀(n : ℕ) → n + 0 ≡ n
rightunit = {!!}

assoc : ∀(a b c : ℕ) → (a + b) + c ≡ a + (b + c)
assoc = {!!}

pred : ℕ → ℕ
pred = {!!}

------------------------------------------------------------------------------
-- Booleans
------------------------------------------------------------------------------

-- -----------    ------------
-- true : Bool    false : Bool
-- 
-- b : Bool    t : A    f : A
-- --------------------------
--   if b then t else f : A
--
-- if true then t else f = t
-- if false then t else f = t

-- and, or, xor with proofs

-- P : Bool → Set    t : P true    f : P false    b : Bool
-- -------------------------------------------------------
--                 Ifthenelse P t f b : P b
-- 
-- Ifthenelse P t f true = t
-- Ifthenelse P t f false = f

-- true ≠ false
