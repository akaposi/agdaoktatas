{-# OPTIONS --rewriting #-}

module Ex where

open import lib2 hiding (Bool; true; false; if_then_else_; three)

-- Propositional logic

-- _→_

l1 : X → X
l1 = {!!}

l2 : X → Y → X
l2 = {!!}

l3 : (X → Y) → X
l3 = {!!}

l4 : X → Y → Y
l4 = {!!}

l5 : X → X → X
l5 = {!!}

l5' : X → X → X
l5' = {!!}

l6 : (X → Y → Z) → (Y → X → Z)
l6 = {!!}

l6b : (X → Y) → (Y → Z) → (X → Z)
l6b = {!!}

l6c : (Y → Z) → (X → Y) → (X → Z)
l6c = {!!}

l6d : (X → X → Y) → (X → Y)
l6d = {!!}

l6e : (X → Y) → (X → X → Y)
l6e = {!!}

l6f : (X → Y) → (X → X → Y)
l6f = {!!}

l16g : (X → Y) → (X → Y)
l16g = {!!}

-- ⊤

l7 : ⊤
l7 = {!!}

l8 : X → ⊤
l8 = {!!}

l8b : ⊤ → ⊤
l8b = {!!}

-- ⊥

l9 : ⊥ → X
l9 = {!!}

l9b : (X → ⊥) → (X → Y)
l9b = {!!}

l9c : (⊤ → ⊥) → ⊥
l9c = {!!}

-- ¬_

l10 : X → ¬ ¬ X
l10 = {!!}

l11 : ¬ ¬ (¬ ¬ X → X)
l11 = {!!}

l12 : ¬ ¬ ¬ X → ¬ X
l12 = {!!}

l13 : ¬ X → ¬ ¬ ¬ X
l13 = {!!}

-- _×_

l14 : (X → Y) × (Y → Z) → (X → Z)
l14 = {!!}

l15 : ((X × Y) × Z) → (X × (Y × Z))
l15 = {!!}

l16 : (X → (Y × Z)) → ((X → Y) × (X → Z))
l16 = {!!}

l16' : (X × Y → Z) → Y × (Y → X) → Z
l16' = {!!}

-- _↔_

l19 : X ↔ X × ⊤
l19 = {!!}

l17 : ¬ ¬ ¬ X ↔ ¬ X
l17 = {!!}

l18 : ¬ (X ↔ ¬ X)
l18 = {!!}

-- _⊎_

l21 : X ⊎ X → X
l21 = {!!}

l22 : (X × Y) ⊎ Z → (X ⊎ Z) × (Y ⊎ Z)
l22 = {!!}

l20 : X ↔ X ⊎ ⊥
l20 = {!!}

l23 : (Y ⊎ X) ↔ (X ⊎ Y)
l23 = {!!}

l24 : (X ⊎ Y → Z) → (X → Z) × (Y → Z)
l24 = {!!}

l25 : ¬ ¬ (X ⊎ ¬ X)
l25 = {!!}

l26 : (¬ X ⊎ Y) → X → Y
l26 = {!!}

l27 : ¬ (X ⊎ Y) ↔ (¬ X × ¬ Y)
l27 = {!!}

l28 : (¬ X ⊎ ¬ Y) → ¬ (X × Y)
l28 = {!!}

-- Propositions as types

-- define all different elements of the following sets

e1 : ⊤ × ⊤
e1 = {!!}

e2 : ⊤ ⊎ (⊤ ⊎ ⊤)
e2 = {!!}

e2' : ⊤ ⊎ (⊤ ⊎ ⊤)
e2' = {!!}

e2'' : ⊤ ⊎ (⊤ ⊎ ⊤)
e2'' = {!!}

-- defining booleans and operations of booleans

Bool : Set
Bool = ⊤ ⊎ ⊤

true false : Bool
true = {!!}
false = {!!}

-- you can ignore {C : Set} for now
if_then_else_ : {C : Set} → Bool → C → C → C
if_then_else_ = {!!}

not : Bool → Bool
not = {!!}

and : Bool → Bool → Bool
and = {!!}

or : Bool → Bool → Bool
or = {!!}

xor : Bool → Bool → Bool
xor = {!!}

-- test the above definitions by C-c C-n

-- define all the elements of Bool × Bool!

bb1 bb2 bb3 bb4 : Bool × Bool
bb1 = {!!}
bb2 = {!!}
bb3 = {!!}
bb4 = {!!}

-- all the functions from the three-element set to Bool

Three : Set
Three = ⊤ ⊎ ⊤ ⊎ ⊤

eight1 eight2 eight3 eight4 eight5 eight6 eight7 eight8 : Three → Bool
eight1 = {!!}
eight2 = {!!}
eight3 = {!!}
eight4 = {!!}
eight5 = {!!}
eight6 = {!!}
eight7 = {!!}
eight8 = {!!}

-- define a case operation on the three-element set!

case3 : {C : Set} → Three → C → C → C → C
case3 = {!!}

-- Peano

two three : ℕ
two = suc (suc zero)
three = suc two

seven : ℕ
seven = suc (suc (suc (suc three)))

-- 0 ↦ 1
-- 1 ↦ 3
-- 2 ↦ 5
-- 3 ↦ 7
-- 4 ↦ 9

2*_+1 : ℕ → ℕ
2*_+1 = λ n → rec (suc zero) (λ x → suc (suc x)) n
{-
  2* three 1+
= 2*_1+ three
= (λ n → rec (suc zero) (λ x → suc (suc x)) n) three
  (λ x → t                                   ) u
  t[x ↦ u]
= rec (suc zero) (λ x → suc (suc x)) three
= rec (suc zero) (λ x → suc (suc x)) (suc (suc (suc zero)))
= (λ x → suc (suc x)) (rec (suc zero) (λ x → suc (suc x)) (suc (suc zero)))
= suc (suc (rec (suc zero) (λ x → suc (suc x)) (suc (suc zero))))
= suc (suc ((λ x → suc (suc x)) (rec (suc zero) (λ x → suc (suc x)) (suc zero))))
= suc (suc ((λ x → suc (suc x)) (rec (suc zero) (λ x → suc (suc x)) (suc zero))))
= suc (suc (suc (suc (rec (suc zero) (λ x → suc (suc x)) (suc zero)))))
= suc (suc (suc (suc (suc (suc (rec (suc zero) (λ x → suc (suc x)) zero))))))
= suc (suc (suc (suc (suc (suc (suc zero))))))
-}

-- check the result of 2* three +1 using C-c C-n

2*_+1' : ℕ → ℕ
2*_+1' = {!!}

-- check the result of 2* three +1' using C-c C-n

four five : ℕ
four = suc three
five = suc four

3*_+5 : ℕ → ℕ
3*_+5 = {!!}

-- test the result of 3* four +5
-- test the result of 3* zero +5

_+_ : ℕ → ℕ → ℕ
_+_ = {!!}

infixl 5 _+_

-- test the result of four + five
-- test the result of five + four
-- test the result of five + three

_*_ : ℕ → ℕ → ℕ
_*_ = {!!}

infixl 6 _*_

-- equality

seventeen : ℕ
seventeen = five + five + five + suc (suc zero)

test1 : 3* four +5 ≡ seventeen
test1 = {!!}

test2 : four + five ≡ five + four
test2 = {!!}

six : ℕ
six = suc five

test3 : three * six ≡ seven + seven + four
test3 = {!!}

test4 : three * four ≡ six + six
test4 = {!!}

twistedTrans : {x y z : ℕ} → x ≡ y → z ≡ x → y ≡ z
twistedTrans = {!!}

-- universal quantification

--                   rec n (λ x → suc x) zero = n
leftunit : (n : ℕ) → zero + n ≡ n
leftunit = {!!}

suc-congruence : (a b : ℕ) → a ≡ b → suc a ≡ suc b
suc-congruence = {!!}

+5-congruence : (a b : ℕ) → a ≡ b → a + five ≡ b + five
+5-congruence = {!!}

twistedTrans' : (A : Set)(x y z : A) → x ≡ y → z ≡ x → y ≡ z
twistedTrans' = {!!}

-- predicates

P : ℕ → Set
P = λ n → n + two ≡ three

pone : P (suc zero) -- = (n + two ≡ three)[n ↦ suc zero] = (three ≡ three)
pone = {!!}

-- induction

--                    rec zero (λ x → suc x) n
rightunit : (n : ℕ) → n + zero ≡ n
rightunit = {!!}

assoc : (a b c : ℕ) → (a + b) + c ≡ a + (b + c)
assoc = {!!}

same : (n : ℕ) → rec zero suc n ≡ n
same = {!!}

sucright : (a b : ℕ) → a + suc b ≡ suc (a + b)
sucright = {!!}

comm : (a b : ℕ) → a + b ≡ b + a -- use rightunit, sucright, sym, trans, cong
comm = {!!}

-- define sym, trans and cong using subst

sym' : (A : Set)(x y : A) → x ≡ y → y ≡ x
sym' = {!!}

trans' : (A : Set)(a a' a'' : A) → a ≡ a' → a' ≡ a'' → a ≡ a''
trans' = {!!}

cong' : (A B : Set)(f : A → B)(a a' : A) → a ≡ a' → f a ≡ f a'
cong' = {!!}

-- define a predicate s.t. Q zero = ⊤ and Q (suc n) = ⊥

Q : ℕ → Set
Q = {!!}

-- show that zero is not equal to one

_≢_ : {A : Set} → A → A → Set
_≢_ = {!!}

0≢1 : zero ≢ suc zero
0≢1 = {!!}

-- similarly show that true is not equal to false. use case instead of
-- if_then_else_

true≢false : true ≢ false
true≢false = {!!}

-- predicate logic

l3b : ¬ ((X Y : Set) → (X → Y) → X)
l3b = {!!}

comp : (A : Set)(B : A → Set)(C : (x : A) → B x → Set)
       (f : (x : A) → B x) → ((x : A) → (y : B x) → C x y)
     → (x : A) → C x (f x)
comp = {!!}

pred1 : {M : Set}{P Q : M → Set}
      → ((x : M) → P x × Q x) ↔ ((x : M) → P x) × ((x : M) → Q x)
pred1 = {!!}

pred2 : {M : Set}{P : M → Set}
      → ¬ ¬ ((x : M) → P x) → (x : M) → ¬ ¬ (P x)
pred2 = {!!}

-- Σ

l3c : Σ Set λ X → Σ Set λ Y → (X → Y) → X
l3c = {!!}

pred3 : {M : Set}{P : M → Set}
   → (Σ M λ x → ¬ P x) → ¬ (∀(x : M) → P x)
pred3 = {!!}

pred4 : {M : Set}{P : M → Set}
   → (¬ Σ M λ x → P x) → ∀(x : M) → ¬ P x
pred4 = {!!}

pred5 : {M : Set}{P : M → Set}
   → ((x : M) → ¬ P x) → ¬ Σ M λ x → P x
pred5 = {!!}

pred6 : {M N : Set}{R : M → N → Set}
   → (Σ M λ x → ∀(y : N) → R x y) → ∀(y : N) → Σ M λ x → R x y
pred6 = {!!}

-- axiom of choice

AC : {M N : Set}{R : M → N → Set}
   → ((x : M) → Σ N λ y → R x y) → Σ (M → N) λ f → (x : M) → R x (f x)
AC = {!!}

-- decidability

Decidable : Set → Set
Decidable = λ A → A ⊎ ¬ A

-- if every set is decidable (law of excluded middle), we have:

dec1 : (lem : (A : Set) → Decidable A) → (A : Set) → ¬ ¬ A → A
dec1 = {!!}

module _ (lem : (A : Set) → Decidable A)(M : Set)(P : M → Set) where

  dec2 : ¬ (X × Y) → (¬ X ⊎ ¬ Y)
  dec2 = {!!}

  Peirce : ((X → Y) → X) → X
  Peirce = {!!}

  dec3 : (¬ ¬ X ⊎ Y) → ¬ Y → X
  dec3 = {!!}

  dec4 : (¬ Σ M λ x → ¬ P x) → (x : M) → P x
  dec4 = {!!}

  Nonempty : Set → Set
  Nonempty = λ M → Σ M λ x → ⊤

  -- use dec4!
  pub : Nonempty M → Σ M λ m → P m → (x : M) → P x
  pub = {!!}

  -- use dec4!
  dec5 : (¬ (∀(x : M) → P x)) → Σ M λ x → ¬ P x
  dec5 = {!!}
